
var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var busboy = require('connect-busboy');
var connect =require('connect');
var Busboy = require('busboy');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use ( favicon() );
app.use ( logger('dev') );
app.use ( busboy() );

//app.use(bodyParser.urlencoded({ extended: false }))
//app.use(bodyParser.json())

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(expressSession({secret:'fucketty fuckfyuck', saveUninitialized: true, resave: true}));
// Routes
var routes = require ('./routes/index');
var users = require ('./routes/users');
var uploadtestpage = require('./routes/uploadtestpage');
var upload = require ('./routes/upload');
var signin = require ('./routes/signin');
var takes = require ('./routes/takes');
var login = require ('./routes/login');
var logout = require ('./routes/logout');
var download = require ('./routes/download');
var stream = require ('./routes/stream');
var deletetake = require ('./routes/deletetake');
var listprores = require ('./routes/listprores');
var removeprores = require ('./routes/removeprores');
var prores = require ('./routes/prores');

app.use('/', routes);
app.use('/users', users);
app.use('/uploadtestpage', uploadtestpage);
app.use('/upload', upload);
app.use('/takes', takes);
app.use('/signin', signin);
app.use('/login', login);
app.use('/logout', logout);
app.use('/download', download);
app.use('/stream', stream);
app.use('/deletetake', deletetake);
app.use('/listprores', listprores);
app.use('/removeprores', removeprores);
app.use('/prores', prores);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    res.render('error404'); 
    nexterr();
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
