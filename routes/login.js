var express = require('express');
var router = express.Router();
var url = require('url');
var fs = require('fs');
var loginsession = require ('../lib/session');
router.post('/', function(req, res) {
		req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
			if (fieldname=="login"){
				console.log("set login"+ val);
				req.session.user=val;
			}
			
			if (fieldname=="password"){
				console.log("set password"+ val);
				req.session.password=val;
			}			
	  	});
	  	
		req.busboy.on('finish', function() {
		    if(loginsession.isUserLogged(req)){
				res.redirect("/takes");
			} else {
				res.render('signin', {message:"#error : user ou password incorrect!"});
			}
		});
		
		req.pipe(req.busboy);
});

module.exports = router;