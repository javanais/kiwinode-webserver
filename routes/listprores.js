var express = require('express');
var fs = require('fs');
var router = express.Router();
var config = require('../config');

/* GET users listing. */
router.get('/', function(req, res) {

	var files = fs.readdirSync(config.fileRoot+"prores");
	var proresfilmArray=[];   
	files.forEach(function(file) {
		if(file.slice(0,1)!="."){
		    console.log(file.slice(0, -4));
		    proresfilmArray.push(file);
		}
	});
	
	
  res.send(JSON.stringify(proresfilmArray));
});

module.exports = router;