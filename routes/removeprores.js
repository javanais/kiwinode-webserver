var express = require('express');
var url = require('url');
var router = express.Router();
var fs = require('fs');
var config = require('../config');

/* GET users listing. */
router.get('/', function(req, res) {
	var url_parts = url.parse(req.url, true);
	console.log(url_parts);
	var filename=url_parts.query["filmname"];
	var filepath=config.fileRoot+"prores/"+ filename+".mov";
	
	if (fs.existsSync(filepath)) {	
	    fs.unlink(filepath);
	    //console.log("file Name : "+filepath+" WILL BE REMOVED");
		res.send(JSON.stringify(["ok"]));
    } else {
	    console.log("file Name :"+filepath+" DOES NOT EXIST");
		res.send(JSON.stringify(["fileNotFound"]));
    }
    
});

module.exports = router;