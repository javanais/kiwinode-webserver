var express = require('express');
var url = require('url');
var router = express.Router();
var fs = require('fs');
var config = require('../config');
var mime = require('mime');


/* GET users listing. */
router.get('/', function(req, res) {
	var url_parts = url.parse(req.url, true);
	//console.log(url_parts.query["filepath"]);
	var relativefilepath=url_parts.query["filepath"];
	var filename=url_parts.query["filename"];
	var filepath = config.fileRoot + relativefilepath + "/" + filename;
	
	if (fs.existsSync(filepath)) {	
		res.setHeader('Content-disposition', 'attachment; filename='+filename);
		
		var mimetype = mime.lookup(filename);
		res.setHeader("content-type", mimetype);
		
		if (mimetype=="video/quicktime"){
			var stat = fs.statSync(filepath);
			var range = req.headers.range;
			
			// res.setHeader("Content-Range", "bytes " + 0 + "-" + stat.size-1 + " / " + stat.size);
			res.setHeader("Accept-Ranges", "bytes");
			res.setHeader("Content-Length", total);
			
			console.log(relativefilepath, "bytes " + 0 + "-" + stat.size-1 + " / " + stat.size);
		}

		var stream = fs.createReadStream(filepath)
		var had_error = false;
		stream.on('open', function () {
    		stream.pipe(res);
		});
		
		stream.on('error', function(err){
			had_error = true;
		});
		
		stream.on('close', function() {
			res.end();
		});
    }else{
	    console.log("stream file Name : "+filepath+" DOES NOT EXIST");
    }

});

module.exports = router;