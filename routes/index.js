var express = require('express');
var router = express.Router();
var loginsession = require ('../lib/session');
var url = require('url');
/* GET home page. */
router.get('/', function(req, res) {
	if( loginsession.isUserLogged(req) ) {
		res.redirect('/takes');
	} else {
		res.redirect('/signin');
	}
});

module.exports = router;
