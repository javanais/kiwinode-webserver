var express = require('express');
var router = express.Router();
var url = require('url');
var fs = require('fs');
var mkdirp = require('mkdirp');
var config = require('../config');
/* GET users listing. */
router.post('/', function(req, res) {
		var fstream;
		var uploadtype = "";
		var machine= "";
		var filemimetype;
		var uploadedFilename;
		
		req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
	      if ( fieldname == "uploadtype" ) {
		      uploadtype=val;
	      }
	      
	      if (fieldname == "machine"){
		      machine=val;
	      }
	  	});

		req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
		    var shouldUpload=true;
		    filemimetype=mimetype;
		    var filePath;
		    uploadedFilename=filename;
		    
		    switch (mimetype) {
		    	case "image/jpeg" :
		    		console.log("thumbnal photo uploaded"+uploadedFilename);
		    		filePath = config.fileRoot+"thumbnail/"
		    		shouldUpload=true;
		    		break;
				case "video/quicktime" :
		    		filePath = config.fileRoot+"tmp/"        
		    		shouldUpload=true;
		    		break;
		    	default:
		    		shouldUpload=false;
		    }
		    
		    if (shouldUpload) {
		   	 	fstream = fs.createWriteStream(filePath+filename);
		   	
		    	file.pipe(fstream);
				fstream.on('close', function () {
					// res.send('uploadtype: ' + req.body.uploadtype);
					file.resume();
				});

		    } else { 
		        console.log("NON a valid format");
		    }
		});
		
		req.busboy.on('finish', function() {
		    if ( filemimetype == "video/quicktime" ) {
			    var destinationfilePath;
			    // console.log('uploadtype : ' + uploadtype);
			    // console.log(uploadtype+" uploaded"+uploadedFilename);
			    switch (uploadtype) {
			    	case "filmjpg" :
			    		destinationfilePath = config.fileRoot+"jpg-photo-film/"
			    		renameMovie (config.fileRoot+"tmp/"+uploadedFilename, destinationfilePath+uploadedFilename);
			    		break;
					case "filmh264" :
			    		//destinationfolderPath = config.h264Folder+ "/" +machine;    
			    		destinationfolderPath = config.h264Folder;    
			    		destinationfilePath = destinationfolderPath +"/"+uploadedFilename;
						console.log("destinationfilePath "+destinationfilePath);
						mkdirp(destinationfolderPath, function(err) { });
			    		renameMovie(config.fileRoot+"tmp/"+uploadedFilename, destinationfilePath);
			    		break;
					case "filmprores" :
			    		destinationfilePath = config.fileRoot+"prores/"        
			    		renameMovie(config.fileRoot+"tmp/"+uploadedFilename, destinationfilePath+uploadedFilename);
			    		break;
			    	case "thumbnailjpg" :
			    		break;
			    	default:
			    		destinationfilePath
			    }
			 }
			 
		    // console.log('uploadtype ' + uploadtype);
			res.send("Done parsing form!");
			res.end();
		});
		
		req.pipe(req.busboy);
});

function renameMovie(origin, destination){
	fs.rename(origin, destination, function(err){
		if (err) { 
			console.log("rename ERROR");		 
		} 
	});
}

module.exports = router;