var express = require('express');
var router = express.Router();
var url = require('url');
var fs = require('fs');
var config = require('../config');
/* POST users listing. */
router.post('/', function(req, res) {
		var takename="";

		req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
			if ( fieldname == "takename" ) {
		    	takename=val;
		    }
	  	});
	  	
	 	req.busboy.on('finish', function() {
	  		if(takename!=""){
	  			if(fs.existsSync(config.fileRoot+"jpg-photo-film/"+takename+".mov")){
			  		console.log("deleting : " + config.fileRoot+"jpg-photo-film/"+takename+".mov");
			  		fs.unlink(config.fileRoot+"jpg-photo-film/"+takename+".mov");
			  	}
			  	
			  	if(fs.existsSync(config.h264Folder+"/"+takename+".mov")){
			  		console.log("delete : " + config.h264Folder+"/"+takename+".mov");
			  		fs.unlink(config.h264Folder+"/"+takename+".mov");
			  	}
			  	
			  	if(fs.existsSync(config.fileRoot+"thumbnail/"+takename+".jpg")){
				  	console.log("delete : " + config.fileRoot+"thumbnail/"+takename+".jpg");		  		
			  		fs.unlink(config.fileRoot+"thumbnail/"+takename+".jpg");
				}
	  		}
	  		res.send("ok");
	  	});
	  	
	  	req.pipe(req.busboy);
});

module.exports = router;