var express = require('express');
var router = express.Router();
var config = require('../config');
var fs = require('fs');
var loginsession = require ('../lib/session');
/* GET home page. */
router.get('/', function(req, res) {
	if ( loginsession.isUserLogged(req) ) {
		//console.log(config.fileRoot+"thumbnail");	
		var files = fs.readdirSync(config.fileRoot+"thumbnail");
		var filmArray=[];  
		
		files.forEach(function(file) {
			if (file.slice(0,1)!=".") {
				// console.log(file.slice(0, -4));
				var takeObject={takename:file.slice(0, -4), thumbnailPath:"thumbnail", h264Path:"film", photojpgPath:"jpg-photo-film"};
				filmArray.push(takeObject);
			}
		});	
		
		res.render('takes',{filmArray:filmArray});
	} else {
		res.redirect('/signin');
	}
});

module.exports = router;
