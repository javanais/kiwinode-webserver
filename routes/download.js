var express = require('express');
var url = require('url');
var router = express.Router();
var fs = require('fs');
var config = require('../config');

/* GET users listing. */
router.get('/', function(req, res) {
	var url_parts = url.parse(req.url, true);
	//console.log(url_parts.query["filepath"]);
	var relativefilepath=url_parts.query["filepath"];
	var filename = url_parts.query["filename"];
	var filepath = config.fileRoot + relativefilepath+"/"+ filename;
	
	if (url_parts.query["type"] == "h264") {
		filepath = config.h264Folder +"/"+ filename; 
	}
	
	if (fs.existsSync(filepath)) {	
		res.setHeader('Content-disposition', 'attachment; filename='+filename);
		res.setHeader("content-type", "application/download");
		var stream = fs.createReadStream(filepath)

		var had_error = false;
		stream.on('open', function () {
    		stream.pipe(res);
		});
		
		stream.on('error', function(err){
			had_error = true;
		});
		
		stream.on('close', function(){
			res.end();
		});
    }else{
	    console.log("file Name :"+filepath+" DOES NOT EXIST");
		//res.render('error', {message:"Le vidéo "+filepath+ "n'exist pas sur le serveur"});
    }
});

module.exports = router;