var express = require('express');
var router = express.Router();
var config = require('../config');
var fs = require('fs');
var loginsession = require ('../lib/session');
/* GET home page. */
router.get('/', function(req, res) {
	if( loginsession.isUserLogged(req) ) {
			var files = fs.readdirSync(config.fileRoot+"prores");
		var filmArray=[];   
		files.forEach(function(file) {
			if(file.slice(0,1)!="."){
			    console.log(file.slice(0, -4));
		    
				var takeObject={takename:file.slice(0, -4), proresPath:"prores"};
				filmArray.push(takeObject);
			}
		});	
	  	res.render('prores',{filmArray:filmArray});
	} else {
		res.redirect('/signin');
	}
});

module.exports = router;
